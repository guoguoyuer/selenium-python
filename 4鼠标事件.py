#引入ActionChains类
from selenium.webdriver.common.action_chains import Action_Chains


#定位到要右击的元素
right = driver.find_element_by_xpath("/html/body/div[4]/div/div/div[4]/div/div[3]/div[2]/div[1]/div[2]/div/ul/li[1]/div/span[1]/input")

#对定位到的元素执行鼠标右键操作
Action_Chains(driver).context_click(right).perform()



#定位到要双击的元素
double = driver.find_element_by_xpath("XXX")

#对定位到的元素执行鼠标双击操作
Action_Chains(driver).double_click(double).perform()



#定位元素的原位置
source = driver.find_element_by_name("XXX")

#定位元素到要移动到的位置
target = driver.find_element_by_name("XXX")

#执行元素的移动操作
Action_Chains(driver).drag_and_drop(source,target).perform()



#定位到鼠标移动到上面的元素
above = driver.find_element_by_xpath("XXX")

#对定位到的元素执行鼠标移动到上面的操作

Action_Chains(driver).move_to_element(above).perform()



#定位到鼠标按下左键的元素

left = driver.find_element_by_xpath("XXX")

#对定位到的元素执行鼠标左键按下的操作

Action_Chains(driver).click_and_hold(left).perform()


