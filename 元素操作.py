#coding=utf-8

from selenium import webdriver

driver = webdriver.Firefox()

driver.get("https://login.live.com/login.srf?wa=wsignin1.0&rpsnv=12&ct=1428569494&rver=6.4.6456.0&wp=MBI_SSL_SHARED&wreply=https:%2F%2Fbay182.mail.live.com%2Fdefault.aspx%3Frru%3Dinbox&lc=2052&id=64855&mkt=zh-cn&cbcxt=mai")


#driver.find_element_by_id("idDiv_PWD_UsernameExample").clear() # 清除输入框默认值
driver.find_element_by_id("idDiv_PWD_UsernameExample").send_keys("111")
# 在输入框输入内容，输入中文：u"中文内容"，若不能解决，将编码声明为GBK
#driver.find_element_by_id("idDiv_PWD_PasswordExample").clear()
driver.find_element_by_id("idDiv_PWD_PasswordExample").send_keys("111")
driver.find_element_by_id("idSIButton9").submit() 
# 通过submit()来提交操作，要求提交对象为表单
# driver.find_element_by_id("idSIButton9").click() 
# 单击按钮，文字/图片链接，按钮，下拉按钮等
# 通过click提交操作

driver.quit()


# size 返回元素大小尺寸
# size = driver.find_element_by_id("kw").size # 返回百度输入框的宽高
# print size

# text 获取元素的文本
# text = driver.find_element_by_id("cp").text # 返回百度页面底部备案信息
# print text

# get_attribute 获得属性值
# attribut = driver.find_element_by_id("kw").get_attribute('type') # 返回元素属性，id，name，type或其他任意属性 
# print attribut

# is_displayed() 是否可见，结果为true or false
# result = driver.find_element_by_id("kw").is_displayed()
# print result